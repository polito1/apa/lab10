/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/21/20
 */

#include "equipArray.h"
#include <stdlib.h>

typedef struct equipArray_s {
    int inUse;
    int objects[EQUIP_SLOT];
} equipArray_s;

// Alloca dinamicamente un equipaggiamento per il personaggio e ne ritorna il puntatore.
equipArray_t equipArray_init()
{
    equipArray_t arr;

    arr = (equipArray_t) malloc(sizeof(equipArray_t));
    arr->inUse = 0;

    return arr;
}

// Libera la memoria occupata dall'equipaggiamento.
void equipArray_free(equipArray_t equipArray)
{
    if (equipArray == NULL) {
        return;
    }

    free(equipArray);
}

// Getter per il campo inUse.
int equipArray_inUse(equipArray_t equipArray)
{
    if (equipArray == NULL) {
        return -1;
    }

    return equipArray->inUse;
}

// Stampa un equipaggiamento.
void equipArray_print(FILE *fp, equipArray_t equipArray, invArray_t invArray)
{
    if (equipArray == NULL || invArray == NULL || fp == NULL) {
        return;
    }

    for (int i = 0; i < equipArray->inUse; i++) {
        invArray_printByIndex(fp, invArray, equipArray->objects[i]);
    }
}

// Aggiunge all'equipaggiamento l'indice dell'oggetto richiesto.
inv_t *equipArray_update(equipArray_t equipArray, invArray_t invArray)
{
    char name[LEN + 1];
    int index;

    if (equipArray == NULL || invArray == NULL) {
        return NULL;
    }

    printf("Inserire il nome dell'oggetto da aggiungere all'inventario: ");
    scanf("%s", name);

    index = invArray_searchByName(invArray, name);

    if (index == -1) {
        printf("Spiacente, oggetto non trovato.\n");
        return NULL;
    }

    equipArray->objects[equipArray_inUse(equipArray)] = index;
    equipArray->inUse ++;

    return invArray_getByIndex(invArray, index);
}

int equipArray_getEquipByIndex(equipArray_t equipArray, int index)
{
    // Controllo di validità dell'indice.
    if (index < 0 || index >= equipArray_inUse(equipArray)) {
        return -1;
    }

    return equipArray->objects[index];
}