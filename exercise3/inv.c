/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/21/20
 */

#include "inv.h"

// Legge una statistica da file.
void stat_read(FILE *fp, stat_t *statp)
{
    if (fp == NULL) {
        return;
    }

    fscanf(
        fp,
        "%d %d %d %d %d %d",
        &statp->hp,
        &statp->mp,
        &statp->atk,
        &statp->def,
        &statp->mag,
        &statp->spr
    );
}

// Stampa una statistica su file.
void stat_print(FILE *fp, stat_t *statp, int soglia)
{
    fprintf(
        fp,
        "%d %d %d %d %d %d",
        statp->hp >= soglia ? statp->hp : soglia,
        statp->mp >= soglia ? statp->mp : soglia,
        statp->atk >= soglia ? statp->atk : soglia,
        statp->def >= soglia ? statp->def : soglia,
        statp->mag >= soglia ? statp->mag : soglia,
        statp->spr >= soglia ? statp->spr : soglia
    );
}

// Legge un oggetto da file.
void inv_read(FILE *fp, inv_t *statp)
{
    if (fp == NULL) {
        return;
    }

    fscanf(fp, "%s %s", statp->nome, statp->tipo);

    stat_read(fp, &(statp->stat));
}

// Stampa un oggetto su file.
void inv_print(FILE *fp, inv_t *invp)
{
    if (invp == NULL) {
        return;
    }

    fprintf(
        fp,
        "%s %s",
        invp->nome,
        invp->tipo
    );

    stat_print(fp, &(invp->stat), MIN_STAT);
}

// Getter per il campo stat dell'oggetto.
stat_t inv_getStat(inv_t *invp)
{
    return invp->stat;
}