/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/21/20
 */

#include "invArray.h"
#include "inv.h"
#include <stdlib.h>

typedef struct invArray_s{
    int length;
    inv_t *objects;
} invArray_s;

// Alloca dinamicamente un array di oggetti.
invArray_t invArray_init()
{
    invArray_t arr;

    arr = (invArray_t) malloc(sizeof(invArray_t));

    return arr;
}

// Libera un array di oggetti.
void invArray_free(invArray_t invArray)
{
    if (invArray == NULL) {
        return;
    }

    free(invArray->objects);
    free(invArray);
}

// Legge da file un array di oggetti.
void invArray_read(FILE *fp, invArray_t invArray)
{
    int length;

    if (fp == NULL) {
        return;
    }

    fscanf(fp,"%d", &length);

    invArray->length = length;
    invArray->objects = (inv_t *) malloc(length * sizeof(inv_t));

    for (int i = 0; i < length; i++) {
        inv_read(fp, &(invArray->objects[i]));
    }
}

// Stampa su file un array di oggetti.
void invArray_print(FILE *fp, invArray_t invArray)
{
    if (fp == NULL) {
        return;
    }

    for (int i = 0; i < invArray->length; i++) {
        inv_print(fp, &(invArray->objects[i]));
        fprintf(fp, "\n");
    }
}

// Stampa su file l'oggetto all'indice index.
void invArray_printByIndex(FILE *fp, invArray_t invArray, int index)
{
    if (index < 0 || index >= invArray->length) {
        return;
    }

    inv_print(fp, invArray_getByIndex(invArray, index));
}

// Ritorna un puntatore all'oggetto all'indice index.
// Ritorna NULL se l'indice non è valido (o non esiste nessun oggetto a quell'indice).
inv_t *invArray_getByIndex(invArray_t invArray, int index)
{
    if (index < 0 || index >= invArray->length) {
        return NULL;
    }

    return &(invArray->objects[index]);
}

// Ricerca lineare di un oggetto.
// Ritorna l'indice dell'oggetto, -1 altrimenti.
int invArray_searchByName(invArray_t invArray, char *name)
{
    for (int i = 0; i < invArray->length; i++) {
        if (strcmp(invArray->objects[i].nome, name) == 0) {
            return i;
        }
    }

    return -1;
}