/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/21/20
 */

#include "pg.h"
#include "equipArray.h"

// Legge da file un personaggio.
int pg_read(FILE *fp, pg_t *pgp)
{
    if (fp == NULL) {
        return 0;
    }

    fscanf(
        fp,
        "%s %s %s",
        pgp->cod,
        pgp->nome,
        pgp->classe
    );

    stat_read(fp, &(pgp->b_stat));

    pgp->equip = equipArray_init();

    return 1;
}

// Stampa su file un personaggio.
void pg_print(FILE *fp, pg_t *pgp)
{
    if (fp == NULL) {
        return;
    }

    fprintf(fp, "%s %s %s ", pgp->cod, pgp->nome, pgp->classe);
    stat_print(fp, &pgp->b_stat, MIN_STAT);
    fprintf(fp, "\n");
}

// Libera l'equipaggiamento, che era stato allocato dinamicamente in precedenza.
void pg_clean(pg_t *pgp)
{
    if (pgp == NULL) {
        return;
    }

    equipArray_free(pgp->equip);
}

// Aggiorna l'equipaggiamento del personaggio.
void pg_updateEquip(pg_t *pgp, invArray_t invArray)
{
    inv_t *added;
    added = equipArray_update(pgp->equip, invArray);

    if (added == NULL) {
        return;
    }

    pgp->b_stat.hp += added->stat.hp;
    pgp->b_stat.mp += added->stat.mp;
    pgp->b_stat.atk += added->stat.atk;
    pgp->b_stat.def += added->stat.def;
    pgp->b_stat.mag += added->stat.mag;
    pgp->b_stat.spr += added->stat.spr;
}