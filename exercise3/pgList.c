/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/21/20
 */

#include "pgList.h"

// Struttura nodo della lista.
typedef struct pg_node{
    pg_t pg;
    struct pg_node *next;
} pg_node;

// Struttura lista.
typedef struct pgList_s {
    pg_node *head;
} pgList_s;

// Alloca dinamicamente e inizializza una lista e ne ritorna il puntatore.
pgList_t pgList_init()
{
    pgList_t list;

    list = (pgList_t) malloc(sizeof(pgList_t));
    list->head = NULL;

    return list;
}

// Libera una lista.
void pgList_free(pgList_t pgList)
{
    pg_node *tmp, *tmp2;

    tmp = pgList->head;

    while (tmp != NULL) {
        tmp2 = tmp;
        tmp = tmp->next;

        // Libera l'equipaggiamento.
        pg_clean(&(tmp2->pg));
        // Libera il nodo.
        free(tmp2);
    }
}

// Stampa una lista su file.
void pgList_print(FILE *fp, pgList_t pgList, invArray_t invArray)
{
    if (fp == NULL) {
        return;
    }

    pg_node *tmp;

    tmp = pgList->head;

    while (tmp != NULL) {
        pg_print(fp, &(tmp->pg));
        tmp = tmp->next;
    }
}

// Inserisce in modo NON ordinato un personaggio in lista.
void pgList_insert(pgList_t pgList, pg_t pg)
{
    pg_node *last, *node;

    if (pgList == NULL) {
        return;
    }

    node = (pg_node *) malloc(sizeof(pg_node));

    node->next = NULL;
    node->pg = pg;

    if (pgList->head == NULL) {
        pgList->head = node;
        return;
    }

    last = pgList->head;

    while (last->next != NULL) {
        last = last->next;
    }

    last->next = node;
}

// Rimuove un personaggio dalla lista.
void pgList_remove(pgList_t pgList, char* cod)
{
    pg_node *tmp, *tmp2;

    tmp = pgList->head;

    while (tmp != NULL) {
        if (strcmp(tmp->next->pg.cod, cod) == 0) {
            tmp2 = tmp->next;
            tmp->next = tmp->next->next;
            pg_clean(&(tmp2->pg));
            free(tmp2);

            return;
        }

        tmp = tmp->next;
    }
}

// Ricerca lineare di un personaggio nella lista dato il suo codice.
pg_t *pgList_searchByCode(pgList_t pgList, char* cod)
{
    pg_node *tmp;

    tmp = pgList->head;

    while (tmp != NULL) {
        if (strcmp(tmp->pg.cod, cod) == 0) {
            return &(tmp->pg);
        }

        tmp = tmp->next;
    }

    return NULL;
}

// Legge una lista da file.
void pgList_read(FILE *fp, pgList_t pgList)
{
    pg_t *tmp;
    if (fp == NULL) {
        return;
    }

    while (!feof(fp)) {
        tmp = (pg_t *) malloc(sizeof(pg_t));

        pg_read(fp, tmp);

        pgList_insert(pgList, *tmp);

        free(tmp);
    }
}