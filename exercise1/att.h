/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/16/20
 */


#ifndef EXERCISE1_ATT_H
#define EXERCISE1_ATT_H

#define MAX_FILENAME_LENGTH 50

typedef struct {
    int start;
    int end;
    int duration;
} att;

int getStart(att a);
int getEnd(att a);
int getDuration(att a);
void sortByEnd(att *a, int length);
void mergeSortR(att *a, att *copy, int start, int end);
void merge(att *a, att *copy, int start, int middle, int end);
int readFromFile(att **v);
void attDump(att a);

#endif //EXERCISE1_ATT_H
