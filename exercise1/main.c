/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 12/16/2020
 */

// Ho cercato di migliorare la struttura e l'eleganza del codice, creando un modulo per le attività.
#ifndef INCLUDE_STDLIBS
#define INCLUDE_STDLIBS

#include <stdio.h>
#include <stdlib.h>

#endif

#include "att.h"

// Definisco le funzioni del modulo main.
void LISDP(att *val, int length);
void LISprint(att *val, int *p, int index);

int main() {
    att *v;
    int nVal;

    nVal = readFromFile(&v);

    // Errore di allocazione o apertura del file.
    if (nVal < 0 || v == NULL) {
        printf("Error.");
        return - 1;
    }

    // Ordino le attività in base alla fine di ognuna di essa, in maniera crescente.
    sortByEnd(v, nVal);

    LISDP(v, nVal);

    free(v);
    return 0;
}

// Ricerca della soluzione utilizzando l'impostazione della LIS.
void LISDP(att *val, int length)
{
    int ris = 1, last = 1, l[length], p[length];

    l[0] = 1;
    p[0] = -1;

    for (int i = 1; i < length; i++) {
        l[i] = 1; p[i] = -1;
        for (int j = 0; j < i; j++) {
            if (getStart(val[i]) >= getEnd(val[j]) && l[i] < l[j] + getDuration(val[j])) {
                l[i] = l[j] + getDuration(val[j]);
                p[i] = j;
            }
        }

        if (ris < l[i]) {
            ris = l[i];
            last = i;
        }
    }

    printf("Sequenza di attività migliore: \n");
    LISprint(val, p, last);
}

// Stampa ricorsiva della soluzione.
void LISprint(att *val, int *p, int index)
{
    if (p[index] == -1) {
        attDump(val[index]);
        return;
    }

    LISprint(val, p, p[index]);
    attDump(val[index]);
}