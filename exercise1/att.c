/**
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created: 12/16/20
 */

#include "att.h"

#ifndef INCLUDE_STDLIBS

#define INCLUDE_STDLIBS
#include <stdlib.h>
#include <stdio.h>

#endif

int getStart(att a)
{
    return a.start;
}

int getEnd(att a)
{
    return a.end;
}

int getDuration(att a)
{
    return a.duration;
}

void sortByEnd(att *a, int length)
{
    int start = 0;
    att *copy = malloc(length * sizeof(att));

    if (copy == NULL) {
        return;
    }

    mergeSortR(a, copy, start, length - 1);

    free(copy);
}

void mergeSortR(att *a, att *copy, int start, int end)
{
    int middle;

    if (start >= end) {
        return;
    }

    middle = (start + end) / 2;

    mergeSortR(a, copy, start, middle);
    mergeSortR(a, copy, middle + 1, end);

    merge(a, copy, start, middle, end);
}

void merge(att *a, att *copy, int start, int middle, int end)
{
    int j, i;

    j = middle + 1;
    i = start;

    for (int k = start; k <= end; k++) {
        if (i > middle) {
            copy[k] = a[j++];
        }

        else if (j > end) {
            copy[k] = a[i++];
        }

        else if (getEnd(a[i]) <= getEnd(a[j])) {
            copy[k] = a[i++];
        }

        else {
            copy[k] = a[j++];
        }
    }
}

int readFromFile(att **v)
{
    FILE *fp;
    char fileName[MAX_FILENAME_LENGTH + 1];
    int nVal;
    att tmp;

    printf("Inserire il nome del file (MAX 50 CARATTERI): ");
    scanf("%s", fileName);

    fp = fopen(fileName, "r");

    if (fp == NULL) {
        v = NULL;
        return -1;
    }

    fscanf(fp, "%d", &nVal);

    // Allocazione dinamica del vettore.
    *v = (att *) malloc(nVal * sizeof(att));

    // Inserisco i valori nel vettore.
    for (int i = 0; i < nVal && !feof(fp); i++) {
        fscanf(fp, "%d %d", &tmp.start, &tmp.end);
        tmp.duration = tmp.end - tmp.start;

        (*v)[i] = tmp;
    }

    // Chiudo il file e ritorno il numero di valori letti.
    fclose(fp);
    return nVal;
}

void attDump(att a)
{
    printf("start: %d, end: %d, duration: %d\n", getStart(a), getEnd(a), getDuration(a));
}
